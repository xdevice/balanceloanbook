# README #

## Coding Challenge: Balance the Loan Books ##


#### Prerequisite ####
* JDK 11 or higher
* Maven

#### Build code ####
Run 

`$ mvn clean install`

Above command would create balanceLoan-1.0-SNAPSHOT-jar-with-dependencies.jar in target directory.

#### Run the jar file ####
Make sure input csv files are in same location as the jar file.

Run 

`$ java -jar balanceLoan-1.0-SNAPSHOT-jar-with-dependencies.jar` 




