package com.balance.loan.model;

import java.math.BigDecimal;

public class Facility {

    Integer bankId;
    Integer facilityId;
    BigDecimal interestRate;
    Integer amount;

    public Facility(Integer bankId, Integer facilityId, BigDecimal interestRate, Integer amount) {
        this.bankId = bankId;
        this.facilityId = facilityId;
        this.interestRate = interestRate;
        this.amount = amount;
    }

    public Integer getBankId() {
        return bankId;
    }

    public Integer getFacilityId() {
        return facilityId;
    }

    public BigDecimal getInterestRate() {
        return interestRate;
    }
    public Integer getAmount() {
        return amount;
    }
    public void setAmount(Integer amount) {
        this.amount = amount;
    }


}
