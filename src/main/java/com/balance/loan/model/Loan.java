package com.balance.loan.model;

import java.math.BigDecimal;

public class Loan {

    Integer id;
    Integer amount;
    BigDecimal interestRate;
    BigDecimal maxDefaultLikelihood;
    String state;
    public Loan(Integer id, Integer amount, BigDecimal interestRate, BigDecimal maxDefaultLikelihood, String state) {
        this.id = id;
        this.amount = amount;
        this.interestRate = interestRate;
        this.maxDefaultLikelihood = maxDefaultLikelihood;
        this.state = state;
    }
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public BigDecimal getInterestRate() {
        return interestRate;
    }

    public void setInterestRate(BigDecimal interestRate) {
        this.interestRate = interestRate;
    }

    public BigDecimal getMaxDefaultLikelihood() {
        return maxDefaultLikelihood;
    }

    public void setMaxDefaultLikelihood(BigDecimal maxDefaultLikelihood) {
        this.maxDefaultLikelihood = maxDefaultLikelihood;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    @Override
    public String toString() {
        return "Loan{" +
                "id=" + id +
                ", amount=" + amount +
                ", interestRate=" + interestRate +
                ", maxDefaultLikelihood=" + maxDefaultLikelihood +
                ", state='" + state + '\'' +
                '}';
    }
}
