package com.balance.loan.model;

import java.math.BigDecimal;

public class Covenant {

    Integer bankId;
    Integer facilityId;
    BigDecimal maxDefaultLikelihood;
    String bannedState;

    public Covenant() {
    }
    public Covenant(Integer bankId, Integer facilityId, BigDecimal maxDefaultLikelihood, String bannedState) {
        this.bankId = bankId;
        this.facilityId = facilityId;
        this.maxDefaultLikelihood = maxDefaultLikelihood;
        this.bannedState = bannedState;
    }
    public Integer getBankId() {
        return bankId;
    }
    public Integer getFacilityId() {
        return facilityId;
    }
    public BigDecimal getMaxDefaultLikelihood() {
        return maxDefaultLikelihood;
    }
    public String getBannedState() {
        return bannedState;
    }

    @Override
    public String toString() {
        return "Covenant{" +
                "bankId=" + bankId +
                ", facilityId=" + facilityId +
                ", maxDefaultLikelihood=" + maxDefaultLikelihood +
                ", bannedState='" + bannedState + '\'' +
                '}';
    }
}
