package com.balance.loan.repository;

import com.balance.loan.model.Facility;
import com.balance.loan.model.Covenant;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;

import java.io.File;
import java.io.FileReader;
import java.io.Reader;
import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

public class CovenantRepository {


    TreeMap<BigDecimal, List<Facility>> interestFacilityMap = new TreeMap<>();
    List<Covenant> covenantList = new ArrayList<>();
    Map<Integer, List<Covenant>> bankIdCovenantMap = new HashMap<>();

    private static CovenantRepository covRepo = new CovenantRepository();
    public static CovenantRepository getInstance() {
        return covRepo;
    }


    public TreeMap<BigDecimal, List<Facility>> getFacilityMap() {
        return interestFacilityMap;
    }

    public Map<Integer, List<Covenant>> getCovenantMap() {
        return bankIdCovenantMap;
    }

    /**
     * Load facilities csv file in memory
     * @param facilities
     * @throws Exception
     */
    public void loadFacilityCSV(File facilities) throws Exception {
        Reader in = new FileReader(facilities);
        Iterable<CSVRecord> records = CSVFormat.DEFAULT
                .withFirstRecordAsHeader().parse(in);
        for (CSVRecord record : records) {
            Integer bankId = Integer.valueOf(record.get("bank_id"));
            Integer facilityId = Integer.valueOf(record.get("id"));
            BigDecimal intRate = new BigDecimal(record.get("interest_rate"));
            //SInce amount is in decimal in csv, convert it into double and then take cast into int
            Integer amount = (int)Double.parseDouble(record.get("amount"));
            Facility facility = new Facility(bankId, facilityId,intRate, amount );
            List<Facility> facilityList = interestFacilityMap.getOrDefault(intRate, new ArrayList<>());
            facilityList.add(facility);
            interestFacilityMap.put(intRate, facilityList);
        }
    }


    /**
     * Load covenant csv file in memory
     * @param covenants
     * @throws Exception
     */
    public void loadCovenantsCSV(File covenants) throws Exception {
        Map<Integer, List<Covenant>> covenantMap = new HashMap<>();
        Reader in = new FileReader(covenants);
        Iterable<CSVRecord> records = CSVFormat.DEFAULT
                .withFirstRecordAsHeader().parse(in);

        for (CSVRecord record : records) {
            Integer bankId = Integer.valueOf(record.get("bank_id"));
            String facilityIdStr = record.get("facility_id");
            Integer facilityId = null;
            if (!facilityIdStr.isEmpty()) {
                facilityId = Integer.valueOf(record.get("facility_id"));
            }
            String defLike = record.get("max_default_likelihood");
            BigDecimal defaultLikelihood;
            if (!defLike.isEmpty()) {
                defaultLikelihood = new BigDecimal(record.get("max_default_likelihood"));
            } else {
                defaultLikelihood = BigDecimal.ZERO;
            }
            String bannedState = record.get("banned_state");
//            covenantList.add(new Covenant(bankId, facilityId, defaultLikelihood, bannedState));
            List<Covenant> covList = covenantMap.getOrDefault(bankId, new ArrayList<>());
            covList.add(new Covenant(bankId, facilityId, defaultLikelihood, bannedState));
            covenantMap.put(bankId, covList);
        }
        //Sort Coventant List for each bankId
        for(Integer bankId: covenantMap.keySet()) {
            List<Covenant> covList = covenantMap.getOrDefault(bankId, new ArrayList<>());
            bankIdCovenantMap.put(bankId,
                    covList.stream()
                    .sorted(Comparator.comparing(Covenant::getMaxDefaultLikelihood).reversed())
                    .collect(Collectors.toList()));
        }

    }



}
