package com.balance.loan;

import com.balance.loan.model.Loan;
import com.balance.loan.service.LoanService;
import com.balance.loan.service.OutPutService;
import com.balance.loan.repository.CovenantRepository;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;

import java.io.File;
import java.io.FileReader;
import java.io.Reader;
import java.math.BigDecimal;
import java.util.concurrent.TimeUnit;

public class StartApp {

    public static void main(String[] args) {
        try {
            var startTime = System.nanoTime();
            File loans = new File("loans.csv");
            File facilities = new File("facilities.csv");
            File covenants = new File("covenants.csv");

            CovenantRepository covRepo = CovenantRepository.getInstance();
            //Load input files
            covRepo.loadFacilityCSV(facilities);
            covRepo.loadCovenantsCSV(covenants);

            processLoans(loans);
            //Write to assignments and yields csv file
            File assignments = new File("assignments.csv");
            File yields = new File("yields.csv");
            OutPutService outService = OutPutService.getOutPutService();
            outService.writeToAssignments(assignments);
            outService.writeToYields(yields);
            var endTime = System.nanoTime();
            var totalDurationMillis = TimeUnit.NANOSECONDS.toMillis(endTime-startTime);
            System.out.println("Finished running the app. Created assignments.csv and yields.csv.");
            System.out.println("Total time taken to run the app: " + totalDurationMillis + " milliseconds");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * Read loans.csv file and process each loan
     * @param loans
     * @throws Exception
     */
    private static void processLoans(File loans) throws Exception {

        LoanService service = LoanService.getInstance();
        Reader in = new FileReader(loans);

        Iterable<CSVRecord> records = CSVFormat.DEFAULT
                .withFirstRecordAsHeader().parse(in);

        for (CSVRecord record : records) {
            Integer loanId = Integer.valueOf(record.get("id"));
            Integer amount = (int)Double.parseDouble(record.get("amount"));
            BigDecimal intRate = new BigDecimal(record.get("interest_rate"));
            BigDecimal defaultLikelihood = new BigDecimal(record.get("default_likelihood"));
            String state = record.get("state");
            Loan loan = new Loan(loanId, amount, intRate, defaultLikelihood, state);

            service.assignLoanToFacility(loan);

        }

    }
}
