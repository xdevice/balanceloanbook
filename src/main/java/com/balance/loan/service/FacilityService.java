package com.balance.loan.service;

import com.balance.loan.model.Covenant;
import com.balance.loan.model.Facility;
import com.balance.loan.repository.CovenantRepository;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

public class FacilityService {

    private static FacilityService facilityService = new FacilityService();
    public static FacilityService getInstance() {
        return  facilityService;
    }

    /**
     *  Returns the facility that has cheapest interest rate and max amount available
     * @param loanAmount
     * @param defaultLiklihood
     * @param loanState
     * @return
     */
    public Facility getFacilityByInterestAndAmount(Integer loanAmount, BigDecimal defaultLiklihood, String loanState) throws Exception {


        TreeMap<BigDecimal, List<Facility>> interestFacilityMap = CovenantRepository.getInstance().getFacilityMap();
        //First try facility with cheapest interest rate
        for(BigDecimal intRate : interestFacilityMap.keySet()) {

            List<Facility> facilityList = interestFacilityMap.get(intRate);
            Optional<Facility> facilityOpt = facilityList.stream()
                    .sorted(Comparator.comparing(Facility::getAmount).reversed()) //sort by reverse order of amount
                    .filter(f -> f.getAmount() >= loanAmount)
                    .filter(f-> {
                        Optional<Covenant> covenant = getCovenantByFacilityId(f.getFacilityId(), f.getBankId(), loanState);
                        return isValidCovenant(covenant, defaultLiklihood, loanState);
                    })
                    .findFirst();
            if(!facilityOpt.isEmpty()) {
                return facilityOpt.get();
            }

        }


        return null;

    }

    /**
     * Returns the covenant to apply on the loan
     * @param facilityId
     * @param bankId
     * @param loanState
     * @return
     */
    public Optional<Covenant> getCovenantByFacilityId(Integer facilityId, Integer bankId, String loanState) {
        //Covenant has bankId and optional facilityId
        //Get Covenant List for given bankId and facilityId
        Map<Integer, List<Covenant>> bankIdCovenantMap = CovenantRepository.getInstance().getCovenantMap();

        //Get Covenant for the bank and filter it by facility id
        List<Covenant> covenantInFacilityAndBank = bankIdCovenantMap.getOrDefault(bankId, new ArrayList<>())
                .stream()
                .filter(c -> c.getFacilityId() == null || c.getFacilityId().equals(facilityId))
                .collect(Collectors.toList());

        //Within this list find if loan state exist in banned state
        Optional<Covenant> covenantOptional = covenantInFacilityAndBank.stream()
                .filter(c -> c.getBannedState().equals(loanState))
                .findFirst();

        if(covenantOptional.isEmpty()) {
            //get covenant if state does not exist in banned state
            covenantOptional =  covenantInFacilityAndBank.stream()
                    .findFirst();
        }

        return covenantOptional;
    }

    /**
     * Loan cannot be assigned to a facility of its either in banned state
     * or default likelihood is greater than the facility can handle
     * @param covenant
     * @param defaultLiklihood
     * @param loanState
     * @return
     */
    public boolean isValidCovenant(Optional<Covenant> covenant, BigDecimal defaultLiklihood, String loanState){
        if(covenant.isEmpty()) {

            return true;
        }
        else {
            Covenant cov = covenant.get();
            if(cov.getBannedState().equals(loanState))
            {
                return false; //Loan is in banned state of facility
            }
            else {
                return (cov.getMaxDefaultLikelihood().compareTo(BigDecimal.ZERO) == 0 ||
                        cov.getMaxDefaultLikelihood().compareTo(defaultLiklihood) >= 0);
            }

        }
    }
}
