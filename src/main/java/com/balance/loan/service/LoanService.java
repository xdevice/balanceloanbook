package com.balance.loan.service;

import com.balance.loan.model.Loan;
import com.balance.loan.model.Facility;
import com.balance.loan.repository.CovenantRepository;

import java.math.BigDecimal;

public class LoanService {

    private static LoanService service = new LoanService();
    public static LoanService getInstance() {
        return service;
    }

    /**
     * Assigns each loan to a facility if possible
     * @param loan
     * @throws Exception
     */
    public void assignLoanToFacility(Loan loan) throws Exception{
        OutPutService outService = OutPutService.getOutPutService();
        FacilityService facilityService = FacilityService.getInstance();
        Facility facility = facilityService.getFacilityByInterestAndAmount(loan.getAmount(), loan.getMaxDefaultLikelihood(), loan.getState());

        if(facility != null) {
            outService.addToAssignments(loan.getId(), facility.getFacilityId());
            Integer facilityAmt = facility.getAmount() - loan.getAmount();
            facility.setAmount(facilityAmt);
            BigDecimal expectYield = getExpectedYield(loan, facility);
            outService.addToYields(facility.getFacilityId(), expectYield);
        }
        else {
            outService.addToAssignments(loan.getId(), null);
        }

    }

    /**
     * Calculates expected yield
     * @param loan
     * @return
     * expected_yield =
     * (1 - default_likelihood) * loan_interest_rate * amount
     * - default_likelihood * amount
     * - facility_interest_rate * amount
     */

    private BigDecimal getExpectedYield(Loan loan, Facility facility) {
        BigDecimal amount = new BigDecimal(loan.getAmount());
        BigDecimal interest = (BigDecimal.ONE.subtract(loan.getMaxDefaultLikelihood()))
                .multiply(loan.getInterestRate())
                .multiply(amount);
        //get expected loss
        BigDecimal loss = loan.getMaxDefaultLikelihood().multiply(amount);
        //expected interest to facility
        BigDecimal interestToFaclity = facility.getInterestRate().multiply(amount);
        //Expected yield on the loan
        BigDecimal expectedYield = interest.subtract(loss).subtract(interestToFaclity);
        return expectedYield;
    }
}
