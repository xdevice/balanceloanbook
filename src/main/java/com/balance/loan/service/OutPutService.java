package com.balance.loan.service;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;

import java.io.File;
import java.io.FileWriter;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class OutPutService {

    private static OutPutService outService = new OutPutService();
    private HashMap<Integer, BigDecimal> facilityTotalYieldsMap = new HashMap<>();
    private List<Assignments> assignmentList = new ArrayList<>();

    public static OutPutService getOutPutService() {
        return outService;
    }

    /**
     * Stores all assignments
     * @param loadId
     * @param facilityId
     */
    public void addToAssignments(Integer loadId, Integer facilityId) {
        assignmentList.add(new Assignments(loadId, facilityId));
    }

    /**
     * Stores faclityId and cumulative yield
     * @param facilityId
     * @param expectedYield
     */
    public void addToYields(Integer facilityId, BigDecimal expectedYield) {
        BigDecimal cumulativeYield = facilityTotalYieldsMap.getOrDefault(facilityId, BigDecimal.ZERO);
        facilityTotalYieldsMap.put(facilityId, cumulativeYield.add(expectedYield));
    }

    /**
     * Write to assignments CSV file
     * @param assignments
     * @throws Exception
     */
    public void writeToAssignments( File assignments) throws Exception {
        String[] HEADERS = { "loan_id", "facility_id"};
        FileWriter out = new FileWriter(assignments);
        try (CSVPrinter printer = new CSVPrinter(out, CSVFormat.DEFAULT
                .withHeader(HEADERS))) {
            for (Assignments assignment : assignmentList) {
                printer.printRecord(assignment.loanId, assignment.facilityId == null ? "" : assignment.facilityId);
            }
        }

    }

    /**
     * Write to yields CSV file
     * @param yields
     * @throws Exception
     */
    public void writeToYields(File yields) throws Exception {
        String[] HEADERS = { "loan_id", "expected_yield"};
        FileWriter out = new FileWriter(yields);
        try (CSVPrinter printer = new CSVPrinter(out, CSVFormat.DEFAULT
                .withHeader(HEADERS))) {
            for (Integer loanId: facilityTotalYieldsMap.keySet()) {
                BigDecimal yield = facilityTotalYieldsMap.get(loanId);
                printer.printRecord(loanId, yield == null ? "" : yield.intValue());
            }
        }
    }

    //Class to store loanId and its assigned facility Id
     private class Assignments {
        Integer loanId;
        Integer facilityId;

         public Assignments(Integer loanId, Integer facilityId) {
             this.loanId = loanId;
             this.facilityId = facilityId;
         }

     }
}
